# -*- coding: utf-8 -*-
from mongoengine import *
import enum
import random
import unittest
from pprint import pprint


class ImagesEnum(enum.Enum):
    cover = 'cover'
    background = 'background'
    foreground = 'foreground'


class QualityEnum(enum.IntEnum):
    LD = 0
    SD = 1
    HD = 2
    FULL_HD = 3


class File(EmbeddedDocument):
    path = StringField()
    quality = IntField()


class Quote(EmbeddedDocument):
    source = StringField()
    text = StringField()


class Episode(EmbeddedDocument):
    num = IntField()
    alias = StringField()
    files = EmbeddedDocumentListField('File')


class Season(Document):
    num = IntField()
    alias = StringField()
    episodes = EmbeddedDocumentListField('Episode', db_field='items')
    meta = {
        'collection': 'products',
        'allow_inheritance': True
    }


class Series(Document):
    title = StringField()
    alias = StringField()
    description = StringField()
    seasons = ListField(ReferenceField('Season'), db_field='items')
    quote = EmbeddedDocumentField('Quote')
    images = MapField(URLField())
    meta = {
        'collection': 'products',
        'allow_inheritance': True
    }


class TestTask(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        connect('test', host='mongo')

    def test_01_create_documents(self):
        def __quote(i):
            source = 'QuoteSource %i' % i
            return {'source': source, 'text': 'test quote'}

        def __images(i):
            return {img.value: 'image path %i' % i for img in ImagesEnum}

        def __files():
            files = list()
            for i in QualityEnum:
                f = File(quality=i, path='file path %i' % i)
                files.append(f)
            return files

        def __episodes():
            episodes = list()
            for i in range(0, random.randint(1, 30)):
                s = Episode(num=i, alias='episode%i' % i, files=__files())
                episodes.append(s)
            return episodes

        def __seasons():
            seasons = list()
            for i in range(0, random.randint(1, 10)):
                s = Season(num=i, alias='season%i' % i, episodes=__episodes())
                s.save()
                seasons.append(s)
            return seasons

        def __series():
            series = list()
            for i in range(0, random.randint(1, 10)):
                s = Series.objects(
                    title='series %i' % i,
                    alias='series%i' % i
                ).modify(
                    upsert=True,
                    new=True,
                    set__quote=__quote(i),
                    set__images=__images(i),
                    set__description='description %i' % i,
                    set__seasons=__seasons())
                series.append(s)
            return series
        self.assertTrue(__series())

    def test_02_create_documents(self):
        """
            Напишите запрос который вернет ответ следующего формата:
            [
              {
                "path": "/series/<alias сериала>",
                "title": "<title сериала>",
                "description": "<description сериала>",
                "cover": "<изображение из поля images с ключем ImagesEnum.cover>",
                "quote": "<значение quote.text>",
                "quote_source": "<значение quote.source>",
                "slide": {
                  "background": "<изображение из поля images с ключем ImagesEnum.background>",
                  "foreground": "<изображение из поля images с ключем ImagesEnum.foreground>"
                }
                "seasons": [
                  {
                    "path": "/series/<alias сериала>/<alias сезона>",
                    "title": "<num сезона> сезон",
                    "episodes": [
                      {
                        "path": "/series/<alias сериала>/<alias сезона>/<alias эпизода>",
                        "title": "Эпизод <num сезона>",
                        "files": [
                          {
                            "path": "<path файла>",
                            "label": "<название enum поля QualityEnum>",
                            "quality": "<значения enum поля QualityEnum>"
                          }
                        ]
                      }
                    ]
                  }
                ]
              }
            ]
        """

        # for serie in self._get_series_by_pipeline():
        for serie in self._get_series2():
            pprint(serie)

    # версия как мне кажется идеомаческая но как то стремная

    def _get_series_by_pipeline(self):
        pipeline = [
            {
                "$project": {
                    "_id": 0,
                    "serie.id": "$_id",
                    "serie.description": "$description",
                    "serie.title": "$title",
                    "serie.path": {"$concat": ["/series/", "$alias"]},
                    "serie.cover": "$images.cover",
                    "serie.quote": "$quote.text",
                    "serie.quote_source": "$quote.source",
                    "serie.slide.background": "$images.background",
                    "serie.slide.foreground": "$images.foreground",
                    "seasons": "$items"
                }
            },
            {

                "$lookup": {
                    "from": "products",
                    "localField": "seasons",
                    "foreignField": "_id",
                    "as": "season"
                }
            },
            {"$unset": "seasons"},
            {"$unwind": {"path": "$season"}},
            {"$project": {
                "_id": 0,
                "serie": 1,
                "season": {
                    "path": {"$concat": ["$serie.path", "/", "$season.alias"]},
                    "title": {"$concat": [{"$toString": "$season.num"}, " сезон"]}
                },
                "episode": "$season.items"
            }
            },

            {"$unwind": {"path": "$episode"}},
            {"$project": {
                "_id": 0,
                "serie": 1,
                "season": 1,
                "episode.path": {"$concat": ["$season.path", "/", "$episode.alias"]},
                "episode.title": {"$concat": ["Эпизод ", {"$toString": "$episode.num"}]},
                "episode.files": {
                    "$map": {
                        "input": "$episode.files",
                        "in": {
                            "path": "$$this.path",
                            "label": {"$arrayElemAt": [["LD", "SD", "HD", "FILL_HD"], "$$this.quality"]},
                            "quality": "$$this.quality"
                        }
                    }}
            }
            },
            # --------------------
            {
                "$group": {
                    "_id": {"serie": "$serie", "season": "$season"},
                    "episodes": {"$push": "$episode"}
                }
            },
            {
                "$project": {
                    "_id": 0,
                    "serie": "$_id.serie",
                    "season.path": "$_id.season.path",
                    "season.title": "$_id.season.title",
                    "season.episodes": "$episodes"
                }
            },
            # --------------------
            {
                "$group": {
                    "_id": {"serie": "$serie"},
                    "seasons": {"$push": "$season"}
                }
            },
            {
                "$project": {
                    "_id": 0,
                    "description": '$_id.serie.description',
                    'cover': '$_id.serie.cover',
                    'path': '$_id.serie.path',
                    'quote': '$_id.serie.quote',
                    'slide': '$_id.serie.slide',
                    'quote_source': '$_id.serie.quote_source',
                    'title': '$_id.serie.title',
                    "seasons": 1,
                }
            }
        ]
        series = Series.objects().aggregate(pipeline)

        return series

    # версия почище но aggregation framework по минимуму

    def _get_series2(self):
        pipeline = [
            {
                "$lookup": {
                    "from": "products",
                    "localField": "items",
                    "foreignField": "_id",
                    "as": "seasons"
                }
            }
        ]

        series = Series.objects().aggregate(pipeline)
        for serie in series:
            serie_path = "/series/{0}".format(serie["alias"])
            serie_images = serie.get("images", {})
            serie_quote = serie.get("quote", {})
            yield {
                "path": serie_path,
                "title": serie.get("title"),
                "description": serie.get("description"),
                "cover": serie_images.get("cover"),
                "quote": serie_quote.get("text"),
                "quote_source": serie_quote.get("source"),
                "slide": {
                    "background": serie_images.get("background"),
                    "foreground": serie_images.get("foreground")
                },
                "seasons": list(
                    {
                        "title": "{0} сезон".format(season.get("num")),
                        "path": "/series/{0}/{1}".format(serie.get("alias"),
                                                         season.get("alias")),
                        "episodes": list(
                            {
                                "path": "/series/{0}/{1}/{2}".format(serie.get("alias"),
                                                                     season.get("alias"),
                                                                     episode.get("alias")),
                                "title": "Эпизод {0}".format(episode.get("num")),
                                "files": list(
                                    {
                                        "path": file.get("path"),
                                        "label": file.get("quality"),
                                        "quality": QualityEnum(file.get("quality")).name
                                    } for file in episode.get("files", [])
                                )
                            } for episode in season.get("items", [])
                        )
                    } for season in serie.get("seasons", [])
                )
            }


if __name__ == '__main__':
    unittest.main()
